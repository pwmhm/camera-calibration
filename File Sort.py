import glob
import os
import shutil
import random

def separate_stereo(source_file) :
    dest = os.path.split(source_file)
    dest_left = os.path.join(dest[0], "Left")
    if not os.path.exists(dest_left) :
        os.makedirs(dest_left)
    dest_right = os.path.join(dest[0], "Right")
    if not os.path.exists(dest_right) :
        os.makedirs(dest_right)
    for filename in glob.glob(source_file) :
        sp = filename.split("_")
        if int(sp[2]) == 0 :
            shutil.move(filename,dest_left)
        else :
            shutil.move(filename,dest_right)


def create_list_stereo(path, depth = "", save = True) :
    dir_left = []
    dir_right = []
    comp_list = []
    path_left = os.path.join(path, "Left", "*.png")

    for filename in glob.glob(path_left) :
        fn=filename.split("\\")
        fn_l=os.path.join(fn[4],fn[5],fn[6])

        split = fn[6].split("_")
        split[2] = "1"
        fn_x = "_".join(split)

        fn_r = os.path.join(fn[4], "Right", fn_x)
        dir_left.append(fn_l)
        dir_right.append(fn_r)

    #check if corresponding depth exists
    if not depth == "" :
        i = 0
        sum = 0
        for filename in glob.glob(os.path.join(depth, "*.png")) :
            fn = os.path.split(filename)[1]
            fx = dir_left[i].split("\\")[2]
            if fn == fx :
                sum += 1
            i += 1


    for i in range(len(dir_left)) :
        a = dir_left[i] + " " + dir_right[i]
        comp_list.append(a)

    random.shuffle(comp_list)
    random.shuffle(comp_list)
    random.shuffle(comp_list)

    for ele in comp_list :
        print(ele)

    train = comp_list[:2569]
    vt = comp_list[2569:]
    valid = vt[:len(vt)//2]
    test = vt[len(vt)//2:]


    if save == True :
        f = open("M:\\CODE\\FAL_net\\Datasets\\vaccinium_train.txt", "w")
        for lines in train :
            f.writelines(lines + "\n")
        f.close()

        f = open("M:\\CODE\\FAL_net\\Datasets\\vaccinium_valid.txt", "w")
        for lines in valid :
            f.writelines(lines + "\n")
        f.close()

        f = open("M:\\CODE\\FAL_net\\Datasets\\vaccinium_test.txt", "w")
        for lines in test :
            f.writelines(lines + "\n")
        f.close()

    return train, valid

dir = "M:\\CODE\\Dataset\\Vaccinium\\Color"
depth_dir = "M:\\CODE\\Dataset\\Vaccinium\\Depth\\*.png"

create_list_stereo(dir, save=True)